import React, { Component } from 'react';
import Aux from './../Aux/Aux';
import { connect } from 'react-redux';
import Header from '../../components/header/Header';

export class Layout extends Component {
    render() {
        return(
            <Aux>

                { this.props.isAuthenticated && <Header /> }

                <main>
                    {this.props.children}
                </main>
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.isAuthenticated
    };
};

export default connect( mapStateToProps )( Layout );