import React, { Component } from 'react';
import Toast from '../../components/toast/Toast';


export class ToastParent extends Component {
    componentDidMount () {
        this._child.notify(this.props.text);
    }

    render () {
        return (
            <div>
                <Toast ref={(child) => { this._child = child; }} />
            </div>
        );
    }
}