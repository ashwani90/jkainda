
//This function  check for the existence of a product in the cart
//if present returns the quantiy otherwise returns the value as false
export const findProductInCart = (cart, sku) => {

    let givenSku = sku;
    let foundProduct = cart.filter(productData => productData.product.sku === givenSku);

    if (foundProduct.length === 0) {
        return false;
    }
    foundProduct = foundProduct[0];

    //this index being passed needs to be changed to something else
    cart.splice(cart.indexOf(foundProduct), 1);

    foundProduct.quantity = foundProduct.quantity + 1;
    cart.push(foundProduct);

    return cart;
};

