import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SpinnerComp } from "../../components/spinner/SpinnerComp";
import { CartCardItem } from '../../components/products/CartCardItem';
import { Row, Col, Button } from 'reactstrap';
import { Redirect } from 'react-router-dom';

export class Cart extends Component {

    constructor(props) {
        super(props);

    }

    state = {
        products: null,
        cartData: null,
        goOrder: null,
        setRequestTimeOut: null
    };

    static getDerivedStateFromProps(nextProps, prevState) {

        if(prevState.products !== nextProps.products) {
            return {
                ...prevState,
                products: nextProps.products,
                cartData: nextProps.cartData,
            }
        }

        if(nextProps.cartData !== prevState.cartData) {
            return {
                ...prevState,
                cartData: nextProps.cartData,
                products: nextProps.products
            }
        }

        return prevState;
    }

    findProductObject = (step, index) => {
      return (
          <Col md="4" xs="6" className="mb-2" key={step.productSku}>
          <CartCardItem { ...step } />
          </Col>
    )
    };

    placeOrder = () => {
        this.setState(
            {
                ...this.state,
                goOrder: true
            }
        )
    };

    setRequestTimeOut = () => {
        setTimeout(() => this.setState(
            {
                ...this.state,
                requestTimedOut: true
            }
        ), 10000)
    }

    render () {

        if(this.state.goOrder) {
            return (
                <Redirect to="/orderDetails" />
            )
        }

        if (this.state.products && this.state.cartData) {
            return (
                <div>
                    <Row className="m-1">
                    {this.state.cartData.map(this.findProductObject, this)}
                    </Row>
                    <Button color="primary" onClick={() => this.placeOrder()}>Order Now!</Button>
                </div>
            );
        }

        if (this.state.cartData === null && this.state.products) {
            return (
                <div>
                    The cart is empty
                </div>
            );
        }

        if (this.state.requestTimedOut) {
            return <Redirect to="/"/>
        }

        {this.setRequestTimeOut()}
        return  <SpinnerComp />;

    }
}

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        products: state.products.productsData,
        cartData: state.cart.cartData
    }
};

export default connect(mapStateToProps)(Cart);
