import React, { Component } from 'react';
import * as actionTypes from '../../store/actions/actionTypes';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';
import { CardItem } from '../../components/products/CardItem';
import { SpinnerComp } from "../../components/spinner/SpinnerComp";
import { Row, Col } from 'reactstrap';
import Toast from "../../components/toast/Toast";
import {ToastParent} from "../../hoc/showToast/showToast";
import { isEquivalent } from '../../shared/utility';

export class Product extends Component {

    state = {
        products: null,
        showToast: null,
        cartData: null
    };

    componentDidMount() {
        this.props.getProducts();
    }

    static getDerivedStateFromProps(nextProps, prevState) {

      if(prevState.products !== nextProps.products) {
          return {
              ...prevState,
              products: nextProps.products,
          }
      }

      if((!(nextProps.cartData === null && prevState.cartData === null))
      || JSON.stringify(nextProps.cartData) !== JSON.stringify(prevState.cartData)) {console.log("Andar aaya");
          return {
              ...prevState,
              cartData: nextProps.cartData,
              showToast: true
          }
      }

      return prevState;
    }

    // shouldComponentUpdate (nextProps, nextState) {
    //     return (nextProps.cartData === null) || JSON.stringify(nextProps.cartData) !== JSON.stringify(nextState.cartData)
    // }

    addProductToCart = (productIndex) => {
        this.setState({
            ...this.state,
            showToast: null
        });

        this.props.addProductToCart(this.state.products[productIndex]);
    };

    render () {

        if(this.state.products !== null) {
            return (
                <div>
                    { (this.state.showToast && <ToastParent text="A Product was added to cart"/> )}
                    <Row className="m-2">
                    {this.state.products.map((step, index) => {
                        return (<Col md="4" xs="6" className="mb-2" key={step.id}>
                            <CardItem { ...step } key={step.id} indexValue={index}
                                      addToCart={(index) => this.addProductToCart(index)}
                            />
                        </Col>)
                    })}
                    </Row>
                </div>
            );
        }

        return <SpinnerComp />

    }
}


const mapStateToProps = state => {
    return {
        products: state.products.productsData,
        cartData: state.cart.cartData
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getProducts: () => dispatch(actions.getProducts()),
        addProductToCart: (sku) => dispatch(actions.addProductToCart(sku))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Product);
