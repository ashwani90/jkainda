import React, { Component, Fragment } from 'react';
import { LoginForm } from "../../components/Form/LoginForm";
import { Button } from "reactstrap";
import { RegisterForm } from "../../components/Form/RegisterForm";
import * as actions from '../../store/actions/index';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

export class Authentication extends Component {

    handleLogin = (data) => {
        this.props.onAuth(data);
    };

    handleRegister = (data) => {
        console.log(data);
    };

    state = {
      switchText: "Switch to Sign Up",
      isSignUp: false
    };

    handleSwitch = () => {
        let switchTextValue = this.state.isSignUp ? 'Switch to Sign Up' : 'Switch to Sign In';
        this.setState(
            {
                ...this.state,
                switchText: switchTextValue,
                isSignUp: !this.state.isSignUp
            }
        )
    };

    render () {
        return (
            <Fragment>

                { this.props.isAuthenticated && <Redirect to="/" /> }

                { this.state.isSignUp
                    ? <RegisterForm
                            handleRegister={(data) => this.handleRegister(data)}
                        />
                    : <LoginForm
                            handleLogin={(data) => this.handleLogin(data)}
                        /> }

                <Button color="warning" onClick={this.handleSwitch}>{this.state.switchText}</Button>
            </Fragment>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onAuth: (data) => dispatch(actions.authLogin(data))
    };
};

const mapStateToProps = (state) => {
    return {
      isAuthenticated: state.auth.isAuthenticated
    };
};

export default connect( mapStateToProps, mapDispatchToProps ) ( Authentication );
