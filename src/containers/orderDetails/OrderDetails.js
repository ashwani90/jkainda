import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';
import { OrderDetailsCard } from '../../components/products/OrderDetailsCard';


export class OrderDetails extends Component {

    state = {
      orderData: null
    };

    //function to make an order
    componentDidMount() {
        this.props.makeOrder(this.props.cartData);
    }

    static getDerivedStateFromProps(nextProps, prevState) {

        if(prevState.orderData !== nextProps.orderData) {
            return {
                ...prevState,
                orderData: nextProps.orderData
            }
        }

        return prevState;
    }

    render () {

        if (this.state.orderData) {
            return (<OrderDetailsCard
                    orderId={this.state.orderData.orderId}
                    orderAmount={this.state.orderData.totalPrice}
                    status={this.state.orderData.status}
                    items={this.state.orderData.orderItems}
            />);
        }

        return (
            <div>This is the order details component {console.log(this.props.cartData)}</div>
        );
    }
}

const mapStateToProps = state => {
    return {
        cartData: state.cart.cartData,
        orderData: state.cart.orderData
    };
};

const mapDispatchToProps = dispatch => {
    return {
        makeOrder: (cartData) => dispatch(actions.makeOrder(cartData))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(OrderDetails);