import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    isAuthenticated: true,
    error: null
};

const authFail = (state, action) => {
    return updateObject( state, { error: action.error, isAuthenticated: false } );
};

const authSuccess = (state, action) => {
    return updateObject( state, { isAuthenticated: true } );
};

const reducer = (state = initialState, action ) => {
    switch (action.type) {
        case actionTypes.AUTH_FAIL: return authFail(state, action);
        case actionTypes.AUTH_SUCCESS: return authSuccess(state, action);
        default: return state;
    }
};

export default reducer;
