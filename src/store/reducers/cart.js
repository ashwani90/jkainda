import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';
import { findProductInCart } from '../../shared/cartHelperFunctions/cartHelperFunctions';

let initialState = {
    cartData: null,
    error: null,
    normalizeCartData: null
};

const addProductToCart = (state, action) => {

    if(state.cartData === null) {
        let productData = { product: action.productData, quantity: 1 };
        return updateObject(state, { cartData: [productData] })
    }

    let foundProduct = findProductInCart(state.cartData, action.productData.sku);

    if(!foundProduct) {
        let productData = { product: action.productData, quantity: 1 };
        state.cartData.push(productData);

        return updateObject(state, { cartData: state.cartData });
    }

    return updateObject(state, { cartData: foundProduct });
};

const orderSuccess = (state, action) => {
    return updateObject(state, { orderData: action.data });
};

const orderFail = (state, action) => {
    return  updateObject(state, { error: action.error });
};



const reducer = (state=initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_PRODUCT_TO_CART: return addProductToCart(state, action);
        case actionTypes.ORDER_SUCCESS: return orderSuccess(state, action);
        case actionTypes.ORDER_FAIL: return orderFail(state, action);
        default: return state;
    }
};

export default reducer;