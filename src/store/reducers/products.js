import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

let initialState = {
    productsData: null,
    error: null
};

const gotProducts = (state, action) => {
    return updateObject( state, { productsData: action.productsData } );
};

const gotError = (state, action) => {
    return updateObject( state, { productsData: action.productsData } );
};


const reducer = (state = initialState, action) => {
  switch(action.type) {
      case actionTypes.GOT_PRODUCTS: return gotProducts(state, action);
      case actionTypes.ERROR: return gotError(state, action);
      default: return state;
  }
};

export default reducer;