export const AUTH_SUCCESS = "AUTH_SUCCESS";
export const AUTH_FAIL = "AUTH_FAIL";
export const GET_PRODUCTS="GET_PRODUCTS";
export const GOT_PRODUCTS="GOT_PRODUCTS";
export const ERROR = "ERROR";
export const ADD_PRODUCT_TO_CART="ADD_PRODUCT_TO_CART";
export const ORDER_SUCCESS = "ORDER_SUCCESS";
export const ORDER_FAIL = "ORDER_FAIL";