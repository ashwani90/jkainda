import * as actionTypes from './actionTypes';
import axios from 'axios';
import * as generalConstants from '../../constants/GeneralConstants';

export const addProductToCart = (product) => {
    return {
        type: actionTypes.ADD_PRODUCT_TO_CART,
        productData: product
    }
};

export const makeOrder = (cartData) => {
  return dispatch => {
      let userId = localStorage.getItem('userId');
      let apiData = {
          'userId': userId,
          'cartData': cartData
      };

      axios.post(generalConstants.ORDER_URL, {apiData}).then(
        response => {
            dispatch(orderSuccess(response.data));
            console.log("Order was successfull");
            console.log(response.data);
        },
                err => {
                    dispatch(orderFail(err.data));
                    console.log("Some error");
                });
        }
};

export const orderSuccess = (data) => {
    return {
        type: actionTypes.ORDER_SUCCESS,
        data: data
    }
};

export const orderFail = (error) => {
    return {
        type: actionTypes.ORDER_FAIL,
        error: error
    }
};