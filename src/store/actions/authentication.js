import axios from 'axios';
import * as actionTypes from './actionTypes';

export const authLogin = (data) => {
    return (dispatch) => {
        let authData = {
            'email': data.email,
            'password': data.password,
            'api_name': 'login_api'
        };
        let url = 'http://localhost/simple_react_demo/login.php';

        axios.post(url, {authData})
            .then(response => {
                localStorage.setItem('userId', response.data.data.id);
                localStorage.setItem('first_name', response.data.data.first_name);
                localStorage.setItem('last_name', response.data.data.last_name);
                localStorage.setItem('username', response.data.data.username);
                localStorage.setItem('email', response.data.data.email);
                localStorage.setItem('address', response.data.data.address);

                dispatch(authSuccess(response.data.data.id));
            }, (err) => {
                dispatch(authFail(err.data.error));
            });
    }
};

export const authSuccess = (userId) => {
  return {
      type: actionTypes.AUTH_SUCCESS,
      userId: userId
  }
};

export const authFail = (error) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    }
};

