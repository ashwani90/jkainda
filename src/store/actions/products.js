import * as actionTypes from './actionTypes';
import axios from 'axios';
import * as generalConstants from '../../constants/GeneralConstants';


export const getProducts = () => {
    return dispatch => {

        axios.get(generalConstants.GET_PRODUCTS_URL).then(
            response => {
                dispatch(gotProducts(response.data.data));
            },
            error => {
                dispatch(gotError(error.data));
            }
        );

    }
};

export const gotProducts = (data) => {
    return {
        type: actionTypes.GOT_PRODUCTS,
        productsData: data
    };
};

export const gotError = (data) => {
    return {
        type: actionTypes.ERROR,
        error: data
    };
};