import React, { Component } from 'react';
import './App.css';
import Layout from './hoc/Layout/Layout';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';
import asyncComponent from './hoc/asyncComponent';
import { connect } from 'react-redux';

const AsyncAuth = asyncComponent(() => {
    return import('./containers/Authentication/Authentication.js');
});

const AsyncProduct = asyncComponent(() => {
    return import('./containers/product/Product.js');
});

const AsyncAbout = asyncComponent(() => {
    return import('./containers/about/About.js');
});

const AsyncCart = asyncComponent(() => {
    return import('./containers/cart/Cart.js');
});

const AsyncOrderDetails = asyncComponent(() => {
    return import('./containers/orderDetails/OrderDetails.js');
});

export class App extends Component {


    cartData = {
        name: "A man with no name",
        brand: "imagjd",
        image_url: "https://images.unsplash.com/photo-1523405976-1345529cc3f4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1534&q=80",
        id: 1,
        description: 'Some description',
        quantity: 1
    };

  render() {

    let routes = (
          <Switch>
              <Route path="/auth" component={AsyncAuth}/>
              <Route path="/" exact component={AsyncProduct} />
              <Redirect to="/" />
          </Switch>
      );

    if(this.props.isAuthenticated){
        routes = (
            <Switch>
                <Route path="/about" component={AsyncAbout}/>
                <Route path="/viewcart" component={AsyncCart}/>
                <Route path="/" exact component={AsyncProduct} />
                <Route path="/orderDetails" exact component={AsyncOrderDetails} />
                <Redirect to="/" />
            </Switch>
        );
    }

    return (
        <div className="App">
            <Layout>
                {routes}
            </Layout>
        </div>
    );
  }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.isAuthenticated
    };
};

export default withRouter(connect(mapStateToProps)(App));
