import React from 'react';
import { FormGroup, Label, Input, Button } from 'reactstrap';

export const IndaInput = ( { inputId, labelName, inputType, inputName, placeholderValue, handleChange, handleBlur } ) => {
  return (
      <FormGroup>
          <Label for={inputId}>{labelName}</Label>
          <Input type={inputType} name={inputName} id={inputId} placeholder={placeholderValue}
                 onChange={ handleChange }
                 onBlur={ handleBlur } />
      </FormGroup>
  );
};

export const IndaButton = ( { btnClass, buttonText, btnType } ) => {
    return (
        <Button color={btnClass} type={btnType}>{buttonText}</Button>
    );
};

export const IndaInputSubmit = ( { btnClass, buttonText, btnType } ) => {
    return (
        <input type={btnType} className={btnClass} value={buttonText} />
    );
};
