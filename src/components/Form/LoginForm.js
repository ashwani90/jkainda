import React, { Component } from 'react';
import { Formik } from 'formik';
import { IndaInput, IndaButton } from "./FormComponents/FormComponents";

export class LoginForm extends Component {

    handleFormSubmit = (data) => {
      this.props.handleLogin(JSON.parse(data));
    };

    render () {
        return (
            <div>
                <h1>Sign In</h1>
                <Formik
                    initialValues={{ email: '', password: '' }}
                    validate={values => {
                        let errors = {};
                        if (!values.email) {
                            errors.email = 'Required';
                        } else if (
                            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                        ) {
                            errors.email = 'Invalid email address';
                        }
                        return errors;
                    }}
                    onSubmit={(values, { setSubmitting }) => {
                            let data = JSON.stringify(values, null, 2);
                            this.handleFormSubmit(data);
                            setSubmitting(false);
                    }}
                >
                    {({
                          values,
                          errors,
                          touched,
                          handleChange,
                          handleBlur,
                          handleSubmit,
                          isSubmitting,

                      }) => (
                        <form onSubmit={handleSubmit}>
                            <IndaInput
                                inputId="email"
                                labelName="Email"
                                inputType="email"
                                inputName="email"
                                placeholderValue="Enter your email"
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                            />
                            {errors.email && touched.email && errors.email}
                            <IndaInput
                                inputId="password"
                                labelName="Password"
                                inputType="password"
                                inputName="password"
                                placeholderValue="Enter your password"
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                            />
                            {errors.password && touched.password && errors.password}
                            <IndaButton
                                btnClass="primary"
                                buttonText="Login"
                                disabled={isSubmitting} />

                        </form>
                    )}
                </Formik>
            </div>
        );
    }
}