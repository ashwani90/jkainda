import React, { Component } from 'react';
import { Formik } from 'formik';
import { IndaInput, IndaButton } from "./FormComponents/FormComponents";

export class RegisterForm extends Component {
    render () {
        return (
            <div>
                <h1>Sign Up</h1>
                <Formik
                    initialValues={{ username: '', name: '', email: '', password: '', address: '' }}
                    validate={values => {
                        let errors = {};
                        if (!values.email) {
                            errors.email = 'Required';
                        } else if (
                            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                        ) {
                            errors.email = 'Invalid email address';
                        }
                        return errors;
                    }}
                    onSubmit={(values, { setSubmitting }) => {
                            console.log(JSON.stringify(values, null, 2));
                            setSubmitting(false);
                    }}
                >
                    {({
                          values,
                          errors,
                          touched,
                          handleChange,
                          handleBlur,
                          handleSubmit,
                          isSubmitting,

                      }) => (
                        <form onSubmit={handleSubmit}>
                            <IndaInput
                                inputId="username"
                                labelName="Username"
                                inputType="text"
                                inputName="username"
                                placeholderValue="Enter your username"
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                            />
                            {errors.username && touched.username && errors.username}
                            <IndaInput
                                inputId="name"
                                labelName="Name"
                                inputType="text"
                                inputName="name"
                                placeholderValue="Enter your name"
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                            />
                            {errors.name && touched.name && errors.name}
                            <IndaInput
                                inputId="email"
                                labelName="Email"
                                inputType="email"
                                inputName="email"
                                placeholderValue="Enter your email"
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                            />
                            {errors.email && touched.email && errors.email}
                            <IndaInput
                                inputId="password"
                                labelName="Password"
                                inputType="password"
                                inputName="password"
                                placeholderValue="Enter your password"
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                            />
                            {errors.password && touched.password && errors.password}
                            <IndaInput
                                inputId="address"
                                labelName="Address"
                                inputType="text"
                                inputName="address"
                                placeholderValue="Enter your address"
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                            />
                            {errors.address && touched.address && errors.address}
                            <IndaButton
                                btnClass="primary"
                                buttonText="Register"
                                disabled={isSubmitting} />

                        </form>
                    )}
                </Formik>
            </div>
        );
    }
}