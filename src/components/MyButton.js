import React, { Component } from 'react';


class MyButton extends Component {

    state = {
        clicked: false
    };

    handleClick = () => {
      this.setState({
          ...this.state,
          clicked: !this.state.clicked
      });
    };

    render () {
        return (
          <button id="ek_button" onClick={() => this.handleClick()}>Say Hello</button>
        );
    }
}

export default MyButton;