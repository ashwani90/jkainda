import React from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem } from 'reactstrap';
import { Link } from 'react-router-dom';


export default class Header extends React.Component {

    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
        return (
                    <div>
                        <Navbar color="light" light expand="md">
                            <Link to='/'>Home</Link>
                            <NavbarToggler onClick={this.toggle} />
                            <Collapse isOpen={this.state.isOpen} navbar>
                                <Nav className="ml-auto" navbar>
                                    <NavItem>
                                        <Link to="/about">About</Link>
                                    </NavItem>
                                    <NavItem>
                                        <Link to="/getModal">Show Modal</Link>
                                    </NavItem>
                                    <UncontrolledDropdown nav inNavbar>
                                        <DropdownToggle nav caret>
                                            Change Background
                                        </DropdownToggle>
                                        <DropdownMenu right>
                                            <DropdownItem>
                                                Black
                                            </DropdownItem>
                                            <DropdownItem>
                                                Blue
                                            </DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>
                                                Reset
                                            </DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledDropdown>
                                    <NavItem>

                                        <Link to="/viewcart"><img height="30px" width="30px" alt="cart" src={'assets/images/cart.png'} /></Link>

                                    </NavItem>
                                </Nav>
                            </Collapse>
                        </Navbar>
                    </div>
        );
    }
}