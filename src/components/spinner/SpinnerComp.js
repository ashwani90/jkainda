import React from 'react';
import { Spinner } from 'reactstrap';

export const SpinnerComp = () => {
    return (
    <div>
        <Spinner color="primary" />
    </div>
    );
};
