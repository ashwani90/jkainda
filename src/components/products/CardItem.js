import React from 'react';
import { Card, CardText, CardBody, CardLink,
    CardTitle, CardSubtitle, Button } from 'reactstrap';


export const CardItem = (props) => {
    return (
        <div>
            <Card>
                <CardBody>
                    <CardTitle>{props.name}</CardTitle>
                    <CardSubtitle>{props.brand}</CardSubtitle>
                </CardBody>
                <img width="100%" height="200px" src={props.image_url} alt={props.id} />
                <CardBody>
                    <CardText>{props.description}</CardText>
                    <CardLink><Button color="primary" onClick={() => props.addToCart(props.indexValue)}>Add To Cart</Button></CardLink>
                    <CardLink href="#"><Button color="success">Order Now</Button></CardLink>
                </CardBody>
            </Card>

        </div>
    );
};
