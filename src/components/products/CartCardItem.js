import React from 'react';
import { Card, CardText, CardBody, CardLink,
    CardTitle, CardSubtitle, Button } from 'reactstrap';


export const CartCardItem = (props) => {
    return (
        <div>
            <Card>
                <CardBody>
                    <CardTitle>{props.product.name}</CardTitle>
                    <CardSubtitle>{props.product.brand}</CardSubtitle>
                </CardBody>
                <img width="100%" height="200px" src={props.product.image_url} alt={props.product.id} />
                <CardBody>
                    <CardText>{props.product.description}</CardText>
                    <CardText>Quantity: {props.quantity}</CardText>
                </CardBody>
            </Card>

        </div>
    );
};
