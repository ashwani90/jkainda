import React from 'react';
import { Card, CardText, CardBody, CardLink,
    CardTitle, CardSubtitle, Button } from 'reactstrap';
import {CartCardItem} from "./CartCardItem";


export const OrderDetailsCard = ({orderId, orderAmount, status, items}) => {
    return (
        <div>
            <Card>
                <CardBody>
                    <CardTitle>Order Id => {orderId}</CardTitle>
                    <CardSubtitle>Order Price => {orderAmount}</CardSubtitle>
                    <CardSubtitle>Order Status => {status}</CardSubtitle>
                </CardBody>

                <CardBody>
                    {items.map((step, index) => {
                        return (<CartCardItem { ...step } key={step.product.id}/>)
                    })}
                </CardBody>
            </Card>

        </div>
    );
};
