import React, { Component } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


class Toast extends Component {
    notify = (text) => toast(text);

    render(){
        return (
            <div>
                <ToastContainer />
            </div>
        );
    }
}

export default Toast;