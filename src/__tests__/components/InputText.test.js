import React from 'react';
import { shallow } from 'enzyme';
import InputText from '../../components/InputText';

describe('test input text', () => {
    test('test input text', () => {
        const inputText = shallow(
          <InputText />
        );
        expect(inputText).not.toBeNull();
    });

    test('test input text value', () => {
        const inputText = shallow(
            <InputText />
        );
        expect(inputText).toMatchSnapshot();
    });

    test('test `state` to be empty', () => {
        const inputText = shallow(
            <InputText />
        );

        expect(inputText.state().data).toEqual([]);
    });
});
