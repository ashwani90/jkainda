import React from 'react';
import { shallow } from 'enzyme';
import Header from '../../../components/header/Header';

describe("test the header component", () => {
    let header = shallow(<Header />)

   test("the header component", () => {
       expect(header.exists('Link[to="/"]')).toBe(true);
       expect(header.exists('Link[to="/about"]')).toBe(true);
       expect(header.exists('Link[to="/getModal"]')).toBe(true);
       expect(header.exists('Link[to="/viewcart"]')).toBe(true);
   });


   test("the toggle function", () => {
      header.instance().toggle();
      expect(header.state().isOpen).toBe(true);
   });
});