import React from 'react';
import { shallow } from 'enzyme';
import MyButton from "../../components/MyButton";

describe('test button', () => {
    test('test button text', () => {
        const myButton = shallow(
            <MyButton />
        );

        expect(myButton.text()).toEqual("Say Hello");
    });

    test('state of the button changes if the button is clicked', () => {
        const myButton = shallow(
            <MyButton />
        );
        myButton.simulate('click');
        expect(myButton.state().clicked).toEqual(true);
    });
});

