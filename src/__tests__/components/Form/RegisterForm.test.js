import React from 'react';
import { mount } from 'enzyme';
import {RegisterForm} from "../../../components/Form/RegisterForm";

describe('testing login form', () => {
    let registerFormComp;
    beforeEach(() => {
        registerFormComp = mount(<RegisterForm />)
    });

    test("test all the input types to be present", () => {
        expect(registerFormComp.exists('IndaInput[inputId="username"]')).toBe(true);
        expect(registerFormComp.exists('IndaInput[inputId="name"]')).toBe(true);
        expect(registerFormComp.exists('IndaInput[inputId="email"]')).toBe(true);
        expect(registerFormComp.exists('IndaInput[inputId="password"]')).toBe(true);
        expect(registerFormComp.exists('IndaInput[inputId="address"]')).toBe(true);
        expect(registerFormComp.exists('IndaButton[buttonText="Register"]')).toBe(true);
    });



    //Same goes for the register form we will test one of the feature, for practice, and leave the others.

});


describe('tests for login form functionality', () => {
    test("form submission", () => {
        let mockSubmitFunction = jest.fn();
        const loginComponent = mount(<RegisterForm />);
        let formikForm =  loginComponent.find('Formik');
        formikForm.props().initialValues = {
            username: 'ashwani',
            name: 'ashwani',
            email: 'ashwanni90125@gmail.com',
            password: '12345',
            address: 'Somewhere' };
        formikForm.find('form').simulate('submit', {
            preventDefault: () => {} // no op
        });

        let setSubmitting = formikForm.instance().setSubmitting;

        formikForm.props().onSubmit(formikForm.props().initialValues, { setSubmitting } );

    });
});
