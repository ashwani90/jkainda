import React from 'react';
import { shallow } from 'enzyme';
import { IndaInput, IndaButton } from "../../../../components/Form/FormComponents/FormComponents";

describe("test input text", () => {

    let IndaInputComp;
    beforeEach(() => {
        IndaInputComp = shallow(<IndaInput inputId="id" labelName="input_name" inputType="text"
                                           inputName="name" />);
    });

    test("test input text", () => {
        expect(IndaInputComp).not.toBeNull();
    });

    test('test params passed to the input text', () => {
        expect(IndaInputComp.exists('#id')).toBe(true);
    });

    test('test params passed to the input text', () => {
        expect(IndaInputComp.exists('[type="text"]')).toBe(true);
    });

    test('test params passed to the input text', () => {
        expect(IndaInputComp.exists('[name="name"]')).toBe(true);
    });

    test('test params passed to the input text', () => {
        expect(IndaInputComp.exists('[type="text"]')).toBe(true);
    });
});

describe('test button as a form component', () => {
    let IndaButtonComp;
    beforeEach(() => {
        IndaButtonComp = shallow(<IndaButton btnClass="primary" buttonText="input_button" btnType="submit" />);
    });

    test("test input button", () => {
        expect(IndaButtonComp).not.toBeNull();
    });

    test("test input button type", () => {
        expect(IndaButtonComp.exists('[type="submit"]')).toBe(true);
    });

    //there is a text() function in enzyme that can be used for testing of the button text.
});
