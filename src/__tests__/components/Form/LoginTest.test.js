import React from 'react';
import { mount, shallow } from 'enzyme';
import { LoginForm } from "../../../components/Form/LoginForm";

describe('Test the login form', () => {
   test('the login form content', () => {
        const loginComponent = shallow(<LoginForm />);

       expect(loginComponent.find('IndaInput[inputId="email"]')).not.toBeNull();
       expect(loginComponent.find('IndaInput[inputId="password"]')).not.toBeNull();
       expect(loginComponent.find('IndaButton[buttonText="Login"]')).not.toBeNull();
   });

   test("function of the props", () => {
       let mockHandleLogin = jest.fn();
       let data = JSON.stringify({'some': 'data'});
       const loginComponent = shallow(<LoginForm handleLogin={mockHandleLogin} />);
       loginComponent.instance().handleFormSubmit(data);
       expect(mockHandleLogin).toHaveBeenCalledWith(JSON.parse(data));
   });

});

describe('tests for login form functionality', () => {
    test("form submission", () => {
        let mockSubmitFunction = jest.fn();
        const loginComponent = mount(<LoginForm />);
        let formikForm =  loginComponent.find('Formik');
        loginComponent.find("input[id='email']").simulate('change', {  persist: () => {}, target: {
                id: 'email',
                value: 'first@gmail.com',
            }, });
        loginComponent.find("input[id='password']").simulate('change', {  persist: () => {}, target: {
                id: 'password',
                value: '12345',
            }, });
        loginComponent.instance().handleFormSubmit = mockSubmitFunction;
        formikForm.find('form').simulate('submit', {
            preventDefault: () => {} // no op
        });

    });
});
