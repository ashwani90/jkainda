import React from 'react';
import { shallow } from 'enzyme';
import { findProductInCart } from '../../../shared/cartHelperFunctions/cartHelperFunctions';

describe("test the cart helper functions", () => {

    test("when sku is not present", () => {
        let cart = [ { sku: '123', quantity: 1 }];
        let sku = '1234';
        const response = findProductInCart(cart, sku);
        expect(response).toBe(false);

    });

    test("when sku is present", () => {
        let cart = [ { sku: '123', quantity: 1 }];
        let sku = '123';
        const response = findProductInCart(cart, sku);
        expect(response[0].quantity).toEqual(2);
    });

    test.only("when sku is present and multiple product", () => {
        let cart = [ { productSku: '123', quantity: 1 },
                    { productSku: '456', quantity: 1 } ];
        let sku = '456';
        const response = findProductInCart(cart, sku);
        expect(response[1].quantity).toEqual(2);
    });

});
