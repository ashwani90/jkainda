import { isEquivalent } from '../../shared/utility';

describe("test the isEquivalent utility function", () => {
   test("when both values are null", () => {
       let a = null;
       let b = null;
       expect(isEquivalent(a,b)).toBe(false);
   });

    test("when one of the values are null", () => {
        let a = null;
        let b = "sone value";
        expect(isEquivalent(a,b)).toBe(false);
    });

    test("when one of the values are null", () => {
        let a = "Some value";
        let b = null;
        expect(isEquivalent(a,b)).toBe(false);
    });

    test("when both values are some different values", () => {
        let a = [ { productSku: '123', quantity: 1 },
            { productSku: '456', quantity: 1 } ];
        let b = [ { productSku: '123', quantity: 1 },
            { productSku: '456', quantity: 2 } ];
        expect(isEquivalent(a,b)).toBe(false);
    });

    test("when both values are some different values", () => {
        let a = [ { productSku: '123', quantity: 1 },
            { productSku: '456', quantity: 1 } ];
        let b = [ { productSku: '123', quantity: 1 },
            { productSku: '456', quantity: 1 } ];
        expect(isEquivalent(a,b)).toBe(true);
    });


});