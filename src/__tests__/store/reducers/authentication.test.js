import authenticationReducer from '../../../store/reducers/authentication';
import * as actionTypes from '../../../store/actions/actionTypes';

describe('tests the authentication reducer', () => {

    test('the default state', () => {
        const error = 'Some error occured';
        expect(authenticationReducer({
            isAuthenticated: false,
            error: null
        }, { type: 'SOME_ACTION', error }).error).toBeNull();
    });

   test('the auth success reducer', () => {
       const userId = 1;

       expect(authenticationReducer({
           isAuthenticated: false,
           error: null
       }, { type: actionTypes.AUTH_SUCCESS, userId }).isAuthenticated).toEqual(true);
   });

   test('the auth fail reducer', () => {
      const error = 'Some error occured';
      expect(authenticationReducer({
          isAuthenticated: false,
          error: null
      }, { type:actionTypes.AUTH_FAIL, error }).error).not.toBeNull();
   });

});