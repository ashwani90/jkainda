import cartReducer from '../../../store/reducers/cart';
import * as actionTypes from '../../../store/actions/actionTypes';

describe("test the cart reducer function", () => {

    let initialState = {
        cartData: null,
        error: null
    };

   test("the cart reducer function", () => {
       let sku = '123';
       let updatedState = cartReducer(initialState, { type: actionTypes.ADD_PRODUCT_TO_CART, sku});

       expect(updatedState.cartData[0].productSku).toEqual('123');
       expect(updatedState.cartData[0].quantity).toEqual(1);

       //when a different product is added
       sku = "456";
       updatedState = cartReducer(updatedState, { type: actionTypes.ADD_PRODUCT_TO_CART, sku});
       expect(updatedState.cartData[0].productSku).toEqual('123');
       expect(updatedState.cartData[0].quantity).toEqual(1);
       expect(updatedState.cartData[1].productSku).toEqual('456');
       expect(updatedState.cartData[1].quantity).toEqual(1);

       //when a product is added again
       sku = "456";
       updatedState = cartReducer(updatedState, { type: actionTypes.ADD_PRODUCT_TO_CART, sku});

       expect(updatedState.cartData[0].productSku).toEqual('123');
       expect(updatedState.cartData[0].quantity).toEqual(1);
       expect(updatedState.cartData[1].productSku).toEqual('456');
       expect(updatedState.cartData[1].quantity).toEqual(2);

   });
});