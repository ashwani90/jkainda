import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import expect from 'expect';
import * as axios from "axios";
import { authLogin } from '../../../store/actions/index';
import * as actionTypes from '../../../store/actions/actionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock("axios");

describe('async tests ', () => {

    test('check the component data', async () => {
        let resp = { data: {
                data: {
                    id: 1,
                    first_name: 'aash',
                    last_name: 'nd',
                    username: 'ekd',
                    email: 'ssds',
                    address: 'sad'
                }
            } };
        axios.post.mockResolvedValue( resp );

        let inputData = {
            email: 'ashwani90125@gmail.com',
            password: '12345'
        };

        const expectedAction = {
            type: actionTypes.AUTH_SUCCESS
        };

        const store = mockStore();
        await store.dispatch(authLogin(inputData));
        const actions= store.getActions();
        expect(actions[0]).toEqual({ type: 'AUTH_SUCCESS', userId: 1 });

        //console.log(store.dispatch(actions.authLogin(inputData)));
        //return store.dispatch(actions.authLogin(data)).toEqual(expectedAction);

    });

    test("when the credentials are invalid", async () => {
        let resp = {
                data: {
                    error: "some error occured"
                }
             };
        let inputData = {
            email: 'ashwani90125@gmail.com',
            password: '12345'
        };
        axios.post.mockRejectedValue( resp );
        const store = mockStore();
        await store.dispatch(authLogin(inputData));
        const actions= store.getActions();
        expect(actions[0]).toEqual({ type: 'AUTH_FAIL', error: "some error occured" });
    });

});
describe("some test arrangement", () => {
    test('testing redux thunk ', async () => {

        let resp = { data: {
                data: {
                    id: 1,
                    first_name: 'aash',
                    last_name: 'nd',
                    username: 'ekd',
                    email: 'ssds',
                    address: 'sad'
                }
            } };

        axios.post.mockResolvedValue( resp );
        let inputData = {
                email: 'ashwani90125@gmail.com',
                password: '12345'
            };

        const store = mockStore();
        await store.dispatch(authLogin(inputData));
        const actions= store.getActions();
        expect(actions[0]).toEqual({ type: 'AUTH_SUCCESS', userId: 1 });
    })
});
