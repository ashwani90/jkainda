import * as actions from '../../../store/actions/authentication';
import * as actionTypes from '../../../store/actions/actionTypes';


//this tests an action of the redux.
describe('actions', () => {
    test('should create an action for authentication success', () => {
        const userId = '1';
        const expectedAction = {
            type: actionTypes.AUTH_SUCCESS,
            userId
        };
        expect(actions.authSuccess(userId)).toEqual(expectedAction);
    });
    test('should create an action for authentication fail', () => {
        const error = 'Some error occured';
        const expectedAction = {
            type: actionTypes.AUTH_FAIL,
            error
        };
        expect(actions.authFail(error)).toEqual(expectedAction);
    });
});


