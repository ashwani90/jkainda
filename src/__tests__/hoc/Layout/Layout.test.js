import React from 'react';
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { mount } from 'enzyme';
import InputText from "../../../components/InputText";
import Layout from "../../../hoc/Layout/Layout";
import { Layout as Lay } from  "../../../hoc/Layout/Layout";
import configureMockStore from 'redux-mock-store';
import thunk from "redux-thunk";
import { Provider } from 'react-redux';

Enzyme.configure({ adapter: new Adapter() });
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe("test the Layout component", () => {
    let store,wrapper;

    test("layout component rendering", () => {
        let initialState = { auth: {isAuthenticated: false} };
        store = mockStore(initialState);
        wrapper =  mount(<Provider store={store}><Layout> <InputText /> </Layout></Provider>);
        expect(wrapper.exists('InputText')).toBe(true);
    });



});

describe("test instanced properties here", () => {
    let props = { isAuthenticated: true};
    let layout = shallow(<Lay { ...props } />);
    test("the existence of header component", () => {
        expect(layout.exists('Header')).toBe(true);

    });

});
