import React from 'react';
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { mount } from 'enzyme';
import asyncComponent from '../../hoc/asyncComponent';
import InputText from "../../components/InputText"
import renderer from 'react-test-renderer';


Enzyme.configure({ adapter: new Adapter() });

describe("test the higher order component ", () => {
   test('Component is rendered properly', () => {
       let AsyncComponentWrapper = asyncComponent(() => {
           return import('../../components/MyButton.js')
       });

       const wrapper = mount(<AsyncComponentWrapper />);
       expect(wrapper).not.toBe(null);
   });
});