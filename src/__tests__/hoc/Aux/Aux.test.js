import React from 'react';
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { mount } from 'enzyme';
import Aux from '../../../hoc/Aux/Aux';
import InputText from "../../../components/InputText";

Enzyme.configure({ adapter: new Adapter() });

describe("test the aux component", () => {
   test("aux component rendering", () => {
        let props = { children: 'some data'};
       const wrapper = mount(<Aux> <InputText /> </Aux>);

        expect(wrapper.exists('InputText')).toBe(true);
   });
});