import React from 'react';
import { shallow } from 'enzyme';
import { App }  from '../App';
import Adapter from "enzyme-adapter-react-16/build/index";
import Enzyme from "enzyme/build/index";


Enzyme.configure({ adapter: new Adapter() });

describe("test the app component", () => {
   test("app component renders properly", () => {
      const app = shallow(<App />);
      expect(App).not.toBeNull();
   });
});

