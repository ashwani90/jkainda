import React from 'react';
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { mount } from 'enzyme';
import{ Authentication } from "../../../containers/Authentication/Authentication";
import Auth from "../../../containers/Authentication/Authentication";
import configureMockStore from 'redux-mock-store';
import thunk from "redux-thunk";
import { Provider } from 'react-redux';

Enzyme.configure({ adapter: new Adapter() });
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

function setUp() {

    // as we need the inner components so we need to get the component using the mount function as only
    // that way we can the inner components

    const enzymeWrapper = shallow(<Authentication  />)

    return {
        enzymeWrapper
    }
}

describe('testing of the authentcation conatainer', () => {

    let auth = shallow(<Authentication />);

    test('test the initial values in the state', () => {
        expect(auth.state().isSignUp).toBe(false);
        expect(auth.state().switchText).toMatch(/Up/);
        expect(auth.exists('LoginForm')).toBe(true);
    });

    // now we can not test this as we are doing so far.
    // as the component here is wrapped inside the connect function
    // and to test such function we need the test to consider that.
    test("switching of the sign in page to sign up page", () => {
        auth.find('Button').simulate('click');
        expect(auth.state().isSignUp).toBe(true);
        expect(auth.state().switchText).toMatch(/In/);
        expect(auth.exists('RegisterForm')).toBe(true);
        auth.find('Button').simulate('click');
        expect(auth.state().isSignUp).toBe(false);
        expect(auth.state().switchText).toMatch(/Up/);
        expect(auth.exists('LoginForm')).toBe(true);
    });

    test('the helper function inside the component', () => {
       auth.instance().handleSwitch();
       expect(auth.state().isSignUp).toBe(true);
    });

});

describe('tests functions that need some props to be passed', () => {

    let auth, authMockFunction, props;
    beforeEach (() => {
        authMockFunction = jest.fn();
        props = { onAuth: authMockFunction };
        auth =  shallow(<Authentication { ...props } />);
    });

    test("the handle login method",  () => {
        auth.instance().handleLogin();
        expect(authMockFunction).toHaveBeenCalled();
    });

    test("the handle login method",  () => {
        auth.instance().handleRegister();
        //later on we will write some tests for the testing of the register function
    });

    test("the handleRegister method of props", () => {
        let data = {"some": "data"};
        let handleRegisterMock = jest.fn();
        auth.instance().handleLogin = handleRegisterMock;
       auth.find('LoginForm').props().handleLogin(data);

       expect(handleRegisterMock).toHaveBeenCalled();
        auth.find('Button').simulate('click');
        auth.instance().handleRegister = handleRegisterMock;
        auth.find('RegisterForm').props().handleRegister(data);
        expect(handleRegisterMock).toHaveBeenCalled();
    });

});

describe("test the connected component", () => {

   let mockOnAuthFn = jest.fn();
   let props = { onAuth: mockOnAuthFn, isAuthenticated: false };
   let auth =  mount(<Authentication { ...props }/>);
   let data = {"some": "data"};

   test("passed function to the props of the component", () => {
       auth.instance().handleLogin(data);
       expect(mockOnAuthFn).toHaveBeenCalled();
   });

   test("passed redux state to the props", () => {
       expect(auth.props().isAuthenticated).toBe(false);
   })

});

//More than this is just an overdo and not needed
describe('test the connected component', () => {
    let store,wrapper;
    let initialState = { auth: {isAuthenticated: false} };
    store = mockStore(initialState);
    wrapper =  mount(<Provider store={store}><Auth /></Provider>);
    test("the connected component", () => {
        expect(wrapper.instance().mapDispatchToProps).not.toBeNull();
    });
});