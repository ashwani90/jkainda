import React from 'react';
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { mount } from 'enzyme';
import{ Authentication } from "../../../containers/Authentication/Authentication";
import Auth from "../../../containers/Authentication/Authentication";
import configureMockStore from 'redux-mock-store';
import thunk from "redux-thunk";
import { Provider } from 'react-redux';
import { Product } from '../../../containers/product/Product'

Enzyme.configure({ adapter: new Adapter() });
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe("test product component", () => {
   test("default rendering component", () => {

       let mockGetProducts = jest.fn(() => null);
       let props = { getProducts: () => null, products: null, cartData: null };

       const productComp = shallow(<Product { ...props }/>);
       console.log(productComp.state());
   });
});